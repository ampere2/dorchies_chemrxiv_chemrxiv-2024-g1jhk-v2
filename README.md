# Dorchies_ChemRxiv_chemrxiv-2024-g1jhk-v2

Contains input files used to perform the simulations of the article:

Correlating substrates reactivity at electrified interfaces with electrolyte structure in synthetically relevant organic solvent/water mixtures

Florian Dorchies, Alessandra Serva, Astrid Sidos, Laurent Michot, Michael Deschamps, Mathieu Salanne and Alexis Grimaud
*ChemRxiv*, 2024

https://doi.org/10.26434/chemrxiv-2024-g1jhk-v2

The input files can be used with the Gromacs molecular dynamics software, which is available [here](https://www.gromacs.org/).  The differences between the two folders is the use of either scaled (with a factor 0.8) or unitary charges as described in the article.
